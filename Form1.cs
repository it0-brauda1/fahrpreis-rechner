﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private string label1Name = "l1";
        private string tb1Name = "tb1";
        private string label2Name = "l2";
        private string tb2Name = "tb2";
        private string resultLabelName = "result";

        private const string errorTextName = "errorTxt";
        private const float lengthMultiplier = 0.5f;
        private const float durationMultiplier = 0.3f;

        public Form1()
        {
            InitializeComponent();
            
            object s = null;
            EventArgs e = null;
            rb1_CheckedChanged(s, e);
        }

        private void rb1_CheckedChanged(object sender, EventArgs e)
        {
            // Area1
            Control label1 = this.Controls[label1Name];
            Control textBox1 = this.Controls[tb1Name];
            label1.Enabled = true;
            textBox1.Enabled = true;

            // Area2
            Control label2 = this.Controls[label2Name];
            Control textBox2 = this.Controls[tb2Name];
            label2.Enabled = false;
            textBox2.Enabled = false;

            RenderArea1();
        }

        private void rb2_CheckedChanged(object sender, EventArgs e)
        {
            // Area1
            Control label1 = this.Controls[label1Name];
            Control textBox1 = this.Controls[tb1Name];
            label1.Enabled = false;
            textBox1.Enabled = false;

            // Area2
            Control label2 = this.Controls[label2Name];
            Control textBox2 = this.Controls[tb2Name];
            label2.Enabled = true;
            textBox2.Enabled = true;

            RenderArea2();
        }

        private void tb1_TextChanged(object sender, EventArgs e)
        {
            RenderArea1();
        }

        private void tb1_EnabledChanged(object sender, EventArgs e)
        {
            RenderArea1();
        }

        private void RenderArea1()
        {
            try
            {
                Control label1 = this.Controls[label1Name];
                if (label1.Enabled)
                {
                    Control resultLabel = this.Controls[resultLabelName];
                    Control textBox1 = this.Controls[tb1Name];
                    resultLabel.Text = "Ergebnis: " + (long.Parse(textBox1.Text) * lengthMultiplier).ToString();
                }
                ResetErrorText();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                HandleError();
            }
        }

        private void tb2_TextChanged(object sender, EventArgs e)
        {
            RenderArea2();
        }

        private void tb2_EnabledChanged(object sender, EventArgs e)
        {
            RenderArea2();
        }

        private void RenderArea2()
        {
            try
            {
                Control label2 = this.Controls[label2Name];
                if (label2.Enabled)
                {
                    Control resultLabel = this.Controls[resultLabelName];
                    Control textBox2 = this.Controls[tb2Name];
                    resultLabel.Text = "Ergebnis: " + (long.Parse(textBox2.Text) * durationMultiplier).ToString();
                }
                ResetErrorText();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                HandleError();
            }
        }

        private void HandleError()
        {
            Control errorText = this.Controls[errorTextName];
            errorText.Text = "Check your inputs!";
        }

        private void ResetErrorText()
        {
            Control errorText = this.Controls[errorTextName];
            errorText.Text = "";
        }
    }
}
